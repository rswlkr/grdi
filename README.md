GRDI - Global Road Death Index

A project using data to help understand Road Safety and Efficiency globally. 

The GRDI was created by @rswlkr to help to describe the safety of a country's road transport system. The RDI for a country is calculated by taking its % to road deaths globablly and dividing it % of total world population and then normalizing this value between 0 and 1.

The IDEA is to use GRDI to help identify key factors that lead to a safe (and unsafe) road transport systems to enable policy makers to make better informed desicions about their direction. 

The initial FOCUS is to visualize RDIs and general comparisons to other already available country metrics. Eventually, specifc road safety policy and technologies will be compared to give direct actionable data about what changes specfically can have the most impactful, postive effect on road safety. 
Following this, progression (and regression) of road transport systems will be visible over time and powerful insights can be generated on what changes to policy/tech have the most impactful effect.

(Gathering this data is spawning a project in itself to make Laws, Policy and Data more accesisbile globally (insert_link_here))

To Do List:
* [>] - Gather road death and population data for a given time period
* [>] - Caluclate RDIs for all countries with data
* [>] - Visuallize RDI's
* [] - Calculate insights based on other available country metrics
* [] - Visuallize insights
* [] - Gather country policy and technology implementation/usage worldwide
* [] - Rank the importance of each of these factors based on RDI values 
* [] - Implement mechanism to generate insight and show changes over time

An on going task is to improve the quality of input data AND to generate awareness of the project so that insights have a positive effect on Road Safety. 