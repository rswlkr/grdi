import React, { Component } from 'react';
import logo from './logo.svg';
import up from './up.svg';
import down from './down.svg'
import ReactMapboxGl, { Layer, Feature } from "react-mapbox-gl";
import './App.css';

const Map = ReactMapboxGl({
  accessToken: "pk.eyJ1IjoicnN3bGtyIiwiYSI6ImNqMXFna2sxMjAwOTkycW92MGI0a3BuNzAifQ.76PJDusHZx0QR08_bBevLg"
});

class App extends Component {

  constructor(props){
    super(props)
    this.headerRef = React.createRef()
    this.mapRef = React.createRef()

  }

  scrollToTop = () => {
     window.scrollTo(0,this.headerRef.current.offsetTop)

  }

  scrollToBottom = () => {
  console.log("aye")
    window.scrollTo(0,this.mapRef.current.offsetTop)
  }

  render() {
    return (
      <div className="App">
        <header ref={this.headerRef} className="App-header">
          <img  src={logo} className="App-logo" alt="logo"/>
          <p>
            Global Road Death Index
          </p>
          <p>
            Using data to help improve global road safety and efficiency.
          </p>
          <button className="scrollDownButton" onClick={this.scrollToBottom}>
              <img src={down} alt="downArrow"/>
          </button>
        </header>
        <div ref={this.mapRef}>
        <button className="scrollUpButton" onClick={this.scrollToTop}>
            <img src={up} alt="upArrow"/>
        </button>

        <Map
            style="mapbox://styles/rswlkr/cjtafcwer159x1fleh7aruedv"
            center={[40.866667,34.566667]}
            zoom={[2]}
            containerStyle={{
              height: "100vh",
              width: "100vw"
            }}>
          </Map>

          </div>
      </div>

    );
  }
}

export default App;
